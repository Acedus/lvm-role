# LVM Role Documentation

What this role does is autopartition disks according to desired output.

* The following format defines the usage for /vars/main.yml.
* User may define as many PVs as they require.
* User may define as many LVs as they require

-----------------------------------------

**PV_DEFINITIONS**

`- name: <PV_Name> (Optional)`

`  size: <Size in MB> (Required)`

-----------------------------------------

**VG_DEFINITIONS**

` - name: <VG_Name> (Required)`

-----------------------------------------

**LV_DEFINITIONS**

` - name: <LV_Name> (Required)`

`   comprised_of: <VG_Name> (Required)`

`   size: <LV_Size> (Required)`

-----------------------------------------

**/var/main.yml example:**

```
---

pv_definitions:
   - name: "first_pv"
     size: 5000
   - name: "second_pv"
     size: 4000

vg_definitions:
   - name: "vg_one"

lv_definitions:
   - name: lv_one
     comprised_of: "vg_one"
     size: "1G"
```




